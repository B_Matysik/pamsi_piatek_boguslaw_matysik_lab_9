#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <time.h>
#include "macierz.h"

using namespace std;

// ============================================================================

macierz::macierz(int r)
{
	this->rozmiar = r;
	this->ilosc = 0;
	this->wagi = new int* [r];
	for(int i=0;i<r;i++)
		this->wagi[i]=new int[r];
	for(int i=0;i<r;i++)
		for(int j=0;j<r;j++)
		{
			this->wagi[i][j]=-1;
			this->wagi[j][i]=-1;
		}
	for(int i=0;i<r;i++)
	{
		this->wagi[i][i]=-1;
	}
}

// ============================================================================

bool macierz::dodaj_krawedz(int wezel1, int wezel2, int waga)
{
	if(wezel1>=this->rozmiar||wezel2>=this->rozmiar||wezel1==wezel2)
		return false;
	if(this->wagi[wezel1][wezel2]!=-1)
		return false;
	this->wagi[wezel1][wezel1]=wezel1;
	this->wagi[wezel2][wezel2]=wezel2;
	this->wagi[wezel1][wezel2]=waga;
	this->wagi[wezel2][wezel1]=waga;
	return true;
}

// ============================================================================

bool macierz::usun_krawedz(int wezel1, int wezel2)
{
	if(wezel1>=this->rozmiar||wezel2>=this->rozmiar||wezel1==wezel2)
		return false;
	if(this->wagi[wezel1][wezel2]==-1)
		return false;
	this->wagi[wezel1][wezel2]=-1;
	this->wagi[wezel2][wezel1]=-1;
	return true;
}

// ============================================================================

int macierz::wyszukaj(int wezel1, int wezel2)
{
	if(wezel1>this->rozmiar||wezel2>this->rozmiar)
		return -1;
	if(this->wagi[wezel1][wezel1]==-1||this->wagi[wezel2][wezel2]==-1)
		return -1;
	if(wezel1==wezel2)
		return 0;
	return this->wagi[wezel1][wezel2];
}

// ============================================================================

void macierz::wypisz()
{
	for(int i=0;i<this->rozmiar;i++)
	{
		for(int j=0;j<this->rozmiar;j++)
		{
			if(this->wagi[i][j]==-1)
				cout << "  - ";
			else
				cout << setw(3) << this->wagi[i][j] << " ";
		}
		cout << endl;
	}
}

// ============================================================================

bool macierz::zapelnij(int procent)
{
	if(procent>100||procent<0)
		return false;
	int zakres = 100;
	long int liczba_dodanych = 0;
	long int do_dodania = rozmiar*(rozmiar-1)*procent/200;
	srand((unsigned)time(NULL));
	for(int i=0; i<rozmiar&&liczba_dodanych<do_dodania; i++)
		liczba_dodanych += this->dodaj_krawedz(i,i+1,rand()%zakres+10);
	while(liczba_dodanych<do_dodania)
		liczba_dodanych += this->dodaj_krawedz(rand()%rozmiar,rand()%rozmiar,rand()%zakres+10);
	return true;
}