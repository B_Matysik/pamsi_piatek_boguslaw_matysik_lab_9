#include "stdafx.h"
#include <cstdlib>
#include <iostream>
#include "dijkstra.h"

using namespace std;

// ============================================================================

int dijkstra::licz(macierz * argument, int zrodlo, int cel)
{
	int licznik = argument->wielkosc()-1;
	// tablica
	this->tablica = new int [argument->wielkosc()];
	// wezly
	this->wezly = new bool [argument->wielkosc()];
	for(int i=0; i<argument->wielkosc(); i++)
		this->wezly[i] = false;
	// pierwszy krok
	int min = 1000000;
	int min_wezel = zrodlo;
	int waga;
	this->wezly[zrodlo] = true;
	for(int i=0; i<argument->wielkosc(); i++)
	{
		waga = this->tablica[i] = argument->wyszukaj(zrodlo, i);
		if(min>waga && waga>=0 && zrodlo!=i)
		{
			min = waga;
			min_wezel = i;
		}
	}
	// dalsze kroki
	int wezel;
	while(licznik>0)
	{
		this->wezly[min_wezel]=true;
		min = 1000000;
		wezel = min_wezel;
		for(int i=0; i<argument->wielkosc(); i++)
		{		
			waga = argument->wyszukaj(wezel, i);
			if(waga<1)
				continue;
			if(this->tablica[i]==-1||this->tablica[i]>this->tablica[wezel]+waga)
				this->tablica[i] = this->tablica[wezel]+waga;
			if(min>waga && !this->wezly[i])
			{
				min = waga;
				min_wezel = i;
			}
		}
		licznik--;
	}
	// koniec - zwrot
	return this->tablica[cel];
}

// ============================================================================


int dijkstra::licz(sasiedzi * argument, int zrodlo, int cel)
{
	// tablica
	this->tablica = new int [argument->wielkosc()];
	// wezly
	this->wezly = new bool [argument->wielkosc()];
	for(int i=0; i<argument->wielkosc(); i++)
		this->wezly[i] = false;
	// pierwszy krok
	int min = 1000000;
	int min_wezel = zrodlo;
	int waga;
	this->wezly[zrodlo] = true;
	for(int i=0; i<argument->wielkosc(); i++)
	{
		waga = this->tablica[i] = argument->wyszukaj(zrodlo, i);
		if(min>waga && waga>=0 && zrodlo!=i)
		{
			min = waga;
			min_wezel = i;
		}
	}
	// dalsze kroki
	int wezel;
	while(min_wezel!=cel)
	{
		this->wezly[min_wezel]=true;
		min = 1000000;
		wezel = min_wezel;
		for(int i=0; i<argument->wielkosc(); i++)
		{
			if(this->wezly[i])
				continue;			
			waga = argument->wyszukaj(wezel, i);
			if(waga==-1)
				continue;
			if(this->tablica[i]==-1||this->tablica[i]>this->tablica[wezel]+waga)
				this->tablica[i] = this->tablica[wezel]+waga;
			if(min>waga)
			{
				min = waga;
				min_wezel = i;
			}
		}
	}
	// koniec - zwrot
	return this->tablica[cel];
}

