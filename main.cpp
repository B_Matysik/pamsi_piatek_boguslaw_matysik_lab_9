#include "stdafx.h"
#include <cstdlib>
#include <iostream>
#include <time.h>
#include "dijkstra.h"
#include "Timer.h"
//#include "bellmanford.h"

using namespace std;

// ============================================================================

int main()
{
	int ilosc = 500;
	int wynik;

	dijkstra * dij = new dijkstra;
	
	sasiedzi * arg = new sasiedzi(ilosc);
	Timer t;
	
	//arg->usun_krawedz(0,ilosc-1);
	//arg->dodaj_krawedz(0,2,1);
	//arg->dodaj_krawedz(2,1,0);
	//arg->wypisz();
	
	int zapelnienie[4] = { 100,75,50,25 };
	
	//macierz * arg = new macierz(ilosc);


	cout << arg->zapelnij(25);
	cout << "start" << endl;
	t.start();
	for (int i = 0; i < ilosc; i++ )
		wynik = dij->licz(arg, 0, 4 );
	t.stop();
	cout << "Dijkstra: dla zapelnienia " << "25" << " wynik " << wynik << " w czasie: " << t.getElapsedTimeInMilliSec() << endl;

	delete arg;
	//arg = new macierz(ilosc);
	arg = new sasiedzi(ilosc);

	cout << arg->zapelnij(25);
	cout << "start" << endl;
	t.start();
	for (int i = 0; i < ilosc; i++)
		wynik = dij->licz(arg, 0, ilosc - 1);
	t.stop();
	cout << "Dijkstra: dla zapelnienia " << 50 << " wynik " << wynik << " w czasie: " << t.getElapsedTimeInMilliSec() << endl;

	delete arg;
	// arg = new macierz(ilosc);
	arg = new sasiedzi(ilosc);

	cout << arg->zapelnij(75);
	cout << "start" << endl;
	t.start();
	for (int i = 0; i < ilosc; i++)
		wynik = dij->licz(arg, 0, ilosc - 1);
	t.stop();
	cout << "Dijkstra: dla zapelnienia " << 75 << " wynik " << wynik << " w czasie: " << t.getElapsedTimeInMilliSec() << endl;

	delete arg;
	//arg = new macierz(ilosc);
	arg = new sasiedzi(ilosc);

	cout << arg->zapelnij(100);
	cout << "start" << endl;
	t.start();
	for (int i = 0; i < ilosc; i++)
		wynik = dij->licz(arg, 0, ilosc - 1);
	t.stop();
	cout << "Dijkstra: dla zapelnienia " << 100 << " wynik " << wynik << " w czasie: " << t.getElapsedTimeInMilliSec() << endl;

	delete arg;
	//arg = new macierz(ilosc);
	arg = new sasiedzi(ilosc);

	for (int i = 0; i < 4; i++)
	{
		cout << arg->zapelnij(zapelnienie[i]);
		cout << "start" << endl;
		t.start();
		wynik = dij->licz(arg, 0, ilosc-1);
		t.stop();
		cout << "Dijkstra: dla zapelnienia " << zapelnienie[i] << " wynik " << wynik << " w czasie: " << t.getElapsedTimeInMilliSec() << endl;
	}
	


	system("pause");
	return 0;
}

