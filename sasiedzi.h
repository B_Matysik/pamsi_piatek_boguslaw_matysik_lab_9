
struct sasiad
{
	int wezel;
	int waga;
	sasiad * nastepny;
	sasiad * poprzedni;

	sasiad(int, int, sasiad *, sasiad *);
};

// ============================================================================

class sasiedzi
{
	int rozmiar;
	sasiad **wagi;
	int ilosc;
public:
	sasiedzi(int);
	bool dodaj_krawedz(int, int, int);
	bool usun_krawedz(int, int);
	int wyszukaj(int, int);
	int wielkosc() { return rozmiar; }
	void wypisz();
	bool zapelnij(int);
};