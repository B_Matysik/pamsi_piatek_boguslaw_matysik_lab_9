#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <time.h>
#include "sasiedzi.h"

using namespace std;

// ============================================================================

sasiad::sasiad(int wezel, int waga, sasiad * nastepny, sasiad * poprzedni=NULL)
{
	this->waga = waga;
	this->wezel = wezel;
	this->nastepny = nastepny;
	this->poprzedni = poprzedni;
}

// ============================================================================

sasiedzi::sasiedzi(int r)
{
	this->rozmiar = r;
	this->ilosc = 0;
	this->wagi = new sasiad* [r];
	for(int i=0;i<r;i++)
		this->wagi[i]=NULL;
}

// ============================================================================

bool sasiedzi::dodaj_krawedz(int wezel1, int wezel2, int waga)
{
	if(wezel1>=this->rozmiar||wezel2>=this->rozmiar||wezel1==wezel2)
		return false;
	for(sasiad * wskaznik=this->wagi[wezel1]; wskaznik!=NULL; wskaznik=wskaznik->nastepny)
		if(wskaznik->wezel==wezel2)
			return false;
	sasiad * nowy = new sasiad(wezel2,waga,this->wagi[wezel1]);
	this->wagi[wezel1] = nowy;
	nowy = new sasiad(wezel1,waga,this->wagi[wezel2]);
	this->wagi[wezel2] = nowy;
	return true;
}

// ============================================================================

bool sasiedzi::usun_krawedz(int wezel1, int wezel2)
{
	if(wezel1>=this->rozmiar||wezel2>=this->rozmiar||wezel1==wezel2)
		return false;
	for(sasiad * wskaznik=this->wagi[wezel1]; wskaznik!=NULL; wskaznik=wskaznik->nastepny)
		if(wskaznik->wezel==wezel2)
		{
			if(wskaznik->poprzedni!=NULL)
				wskaznik->poprzedni->nastepny = wskaznik->nastepny;
			if(wskaznik->nastepny!=NULL)
				wskaznik->nastepny->poprzedni = wskaznik->poprzedni;
			delete wskaznik;
			break;
		}
	for(sasiad * wskaznik=this->wagi[wezel2]; wskaznik!=NULL; wskaznik=wskaznik->nastepny)
		if(wskaznik->wezel==wezel1)
		{
			if(wskaznik->poprzedni!=NULL)
				wskaznik->poprzedni->nastepny = wskaznik->nastepny;
			if(wskaznik->nastepny!=NULL)
				wskaznik->nastepny->poprzedni = wskaznik->poprzedni;
			delete wskaznik;
			return true;
		}
	return false;
}

// ============================================================================

int sasiedzi::wyszukaj(int wezel1, int wezel2)
{
	if(wezel1>this->rozmiar||wezel2>this->rozmiar)
		return -1;
	if(wezel1==wezel2)
		return 0;
	for(sasiad * wskaznik=this->wagi[wezel1]; wskaznik!=NULL; wskaznik=wskaznik->nastepny)
		if(wskaznik->wezel==wezel2)
			return wskaznik->waga;
	return -1;
}

// ============================================================================

void sasiedzi::wypisz()
{
	for(int i=0; i<this->rozmiar; i++)
	{
		cout << "[" << i << "]"; 
		for(sasiad * wskaznik=this->wagi[i]; wskaznik!=NULL; wskaznik=wskaznik->nastepny)
			cout << " -> [" << setw(3) << wskaznik->wezel << "] " << setw(3) << wskaznik->waga;
		cout << endl;
	}
}

// ============================================================================

bool sasiedzi::zapelnij(int procent)
{
	if(procent>100||procent<0)
		return false;
	int zakres = 100;
	int liczba_dodanych = 0;
	int do_dodania = rozmiar*(rozmiar-1)*procent/200;
	srand((unsigned)time(NULL));
	for(int i=0; i<rozmiar-1&&liczba_dodanych<do_dodania; i++)
		liczba_dodanych += this->dodaj_krawedz(i,i+1,rand()%zakres);
	while(liczba_dodanych<do_dodania)
		liczba_dodanych += this->dodaj_krawedz(rand()%rozmiar,rand()%rozmiar,rand()%zakres);
	return true;
}