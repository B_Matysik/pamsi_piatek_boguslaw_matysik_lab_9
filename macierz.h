
class macierz
{
	int rozmiar;
	int **wagi;
	int ilosc;
public:
	macierz(int);
	bool dodaj_krawedz(int, int, int);
	bool usun_krawedz(int, int);
	int wyszukaj(int, int);
	int wielkosc() { return rozmiar; }
	void wypisz();
	bool zapelnij(int);
};